//
//  Constants.m
//  MassMailing
//
//  Created by Casper Lee on 28/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

#import "Constants.h"

@implementation Constants

#pragma mark Constants
const int FIRST_COLUMN = 0;
const int SECOND_COLUMN = 1;
NSString *DELIMITER = @"\t";

@end
