//
//  CSVReaderWriter.h
//  MassMailing
//
//  Created by Casper Lee on 17/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

/**
 * CSVReaderWriter handles tab delimited CSV input file by default.
 * Data would read in from the specified path and only first two columns
 * are processed and write it to the desired output path
 */

@interface CSVReaderWriter : NSObject

/** -----------------------------------------------------------------------
 * @name Performing Class Specific I/O Operation
 * -----------------------------------------------------------------------
 */

/**
 Open the path with different `FileMode` provided
 
 @param path The path of data source
 @param mode File mode specifies a input or output
 */
- (void)open:(nonnull NSString *)path mode:(FileMode)mode DEPRECATED_MSG_ATTRIBUTE("Please Use CSVReader's readerForPath: for FileModeRead or CSVWriter's writerForPath:] for FileModeWrite");

/**
 Reads a line from the opened input source.
 String Tokens are processed with the specified delimiter,
 fills in a collection representing a row of data
 
 @param columns A mutable array provided to stored processed tokens
 @return Indicates is the operation succeeded
 */
- (BOOL)read:(nonnull NSMutableArray *)columns DEPRECATED_MSG_ATTRIBUTE("Please Use CSVReader's read:");

/**
 Reads a line from the opened input source.
 String Tokens are processed with the specified delimiter,
 fills in 2 string reference representing first 2 columns of a row of data
 
 @param column1 A string pointer provided to stored the value of first column
 @param column2 A string pointer provided to stored the value of second column
 @return Indicates is the opertation successed
 */
- (BOOL)read:(NSMutableString * _Nonnull * _Nonnull)column1 column2:(NSMutableString * _Nonnull * _Nonnull)column2 DEPRECATED_MSG_ATTRIBUTE("Please Use CSVReader's read:column2:");

/**
 Writes a line of string representation with the specified delimiter
 using the given values
 
 @param columns An array of data representing a row of data
 */
- (void)write:(nonnull NSArray *)columns DEPRECATED_MSG_ATTRIBUTE("Please Use CSVWriter's write:");

/**
 * The stream would close and dispose automatically when end of
 * stream has been reached. In case there is a delay in incoming data, stream would able to append
 * additional data to last opened stream.
 */
- (void)close DEPRECATED_MSG_ATTRIBUTE("Please Use CSVReader / CSVWriter's close:");

@end

