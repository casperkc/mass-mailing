//
//  StreamReader.m
//  MassMailing
//
//  Created by Casper Lee on 17/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

#import "StreamReader.h"

#pragma mark Private Interfaces

@interface StreamReader()

@property (strong, readwrite, nullable) NSInputStream *inputStream;

@end

#pragma mark - Implementation

@implementation StreamReader

#pragma mark Public Methods

+ (nonnull instancetype)reader {
    return [[self alloc] init];
}

- (void)openPath:(NSString *)path {
    NSInputStream *inputStream = [NSInputStream inputStreamWithFileAtPath:path];
    [inputStream open];
    self.inputStream = inputStream;
}

- (nonnull NSString *)readLine {
    uint8_t ch = 0;
    NSMutableString *str = [NSMutableString string];
    while ([self.inputStream read:&ch maxLength:1] == 1) {
        if (ch == '\n')
            break;
        [str appendFormat:@"%c", ch];
    }
    
    if (!self.inputStream.hasBytesAvailable) {
        [self close];
    }
    
    // Non mutable copy
    return [NSString stringWithString:str];
}

- (void)close {
    [self.inputStream close];
    [self.inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    self.inputStream = nil; // stream is ivar, so reinit it
}

@end
