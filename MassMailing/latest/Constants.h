//
//  Constants.h
//  MassMailing
//
//  Created by Casper Lee on 28/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

typedef NS_OPTIONS(NSUInteger, FileMode) {
    FileModeRead = 1,
    FileModeWrite = 2
};

extern const int FIRST_COLUMN;
extern const int SECOND_COLUMN;
extern NSString * _Nonnull DELIMITER; // Default sets to tab (\t)

@end
