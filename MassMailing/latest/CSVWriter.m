//
//  CSVWriter.m
//  MassMailing
//
//  Created by Casper Lee on 28/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

#import "CSVWriter.h"
#import "StreamWriting.h"
#import "StreamWriter.h"
#import "Constants.h"

#pragma mark - Private Interface

@interface CSVWriter()

@property (nonatomic, strong) id<StreamWriting> writer;

@end

#pragma mark - Public Methods

@implementation CSVWriter

#pragma mark Initialisers
+ (nonnull instancetype)writerForPath:(nonnull NSString *)path {
    return [[self alloc] initForPath:path];
}

- (nonnull instancetype)initForPath:(nonnull NSString *)path {
    self = [self init];
    if (self) {
        StreamWriter *writer = [StreamWriter writer];
        self.writer = writer;
        [self.writer openPath:path];
    }
    return self;
}

- (void)write:(NSArray *)columns {
    NSMutableString *outPut = [columns componentsJoinedByString:DELIMITER].mutableCopy;
    [outPut appendString:@"\n"];
    NSData *data = [outPut dataUsingEncoding:NSUTF8StringEncoding];
    [self.writer appendData:data];
}

- (void)close {
    [self.writer close];
}

@end
