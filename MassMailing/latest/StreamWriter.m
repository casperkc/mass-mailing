//
//  StreamWriter.m
//  MassMailing
//
//  Created by Casper Lee on 17/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

#import "StreamWriter.h"

#pragma mark - Private Interface

@interface StreamWriter ()

@property (nonatomic, strong) NSString *currentPath;
@property (nullable, strong) NSOutputStream *outputStream;
@property (strong) NSMutableData *dataToWrite;
@property (assign) NSUInteger byteIndex;  // Instance variable to move pointer

@end

#pragma mark - Implementation

@implementation StreamWriter

#pragma mark Public Methods

+ (nonnull instancetype)writer {
    return [[self alloc] init];
}

- (void)openPath:(NSString *)path {
    [self openStream:path resume:NO];
    
    // Reseting data storage and pointer position
    self.dataToWrite = [NSMutableData data];
    self.byteIndex = 0;
}


- (void)appendData:(NSData *)data {
    
    // If stream is disposed, try to append the new data to the last opened path
    if (!self.outputStream && self.currentPath) {
        [self openStream:self.currentPath resume:YES];
    }
    
    if (data.length > 0) {
        @synchronized(self) {
            [self.dataToWrite appendData:data];
        }
    }
}

- (void)close {
    [self.outputStream close];
    [self.outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    self.outputStream = nil;
}

#pragma mark Private Aux Methods

- (void)openStream:(NSString *)path resume:(BOOL)isResume {
    NSOutputStream *outputStream = [NSOutputStream outputStreamToFileAtPath:path append:isResume];
    outputStream.delegate = self;
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream open];
    
    self.currentPath = path;
    self.outputStream = outputStream;
}

#pragma mark - NSStream Protocol

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode {
    
    switch(eventCode) {
        
        case NSStreamEventHasSpaceAvailable:
        {
            // Write buffered data to the path
            uint8_t *readBytes = (uint8_t *)[self.dataToWrite mutableBytes];
            readBytes += self.byteIndex;
            NSUInteger dataLength = [self.dataToWrite length];
            NSUInteger bufferLength = ((dataLength - self.byteIndex >= 1024) ? 1024 : (dataLength - self.byteIndex));
            uint8_t buffer[bufferLength];
            (void)memcpy(buffer, readBytes, bufferLength);
            bufferLength = [self.outputStream write:(const uint8_t *)buffer maxLength:bufferLength];
            self.byteIndex += bufferLength;
            
            // Keep the size of `dataToWrite` but set all the written bytes to zero for better memory performance
            [self.dataToWrite resetBytesInRange:NSMakeRange(0, self.byteIndex - 1)];
            break;
        }
        
        case NSStreamEventEndEncountered:
        case NSStreamEventErrorOccurred:
        {
            [self close];
            break;
        }
            
        default:
            break;
    }
}


@end
