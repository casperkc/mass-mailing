//
//  CSVReader.h
//  MassMailing
//
//  Created by Casper Lee on 28/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface CSVReader : NSObject

/**
 * Creates and returns an `CSVReader` object.
 *
 * @param path The path of data source
 * @return A new `CSVReader` instance
 */
+ (nonnull instancetype)readerForPath:(nonnull NSString *)path;
- (nonnull instancetype)initForPath:(nonnull NSString *)path;

/**
 Reads a line from the opened input source.
 String Tokens are processed with the specified delimiter,
 fills in a collection representing a row of data
 
 @param columns A mutable array provided to stored processed tokens
 @return Indicates is the operation succeeded
 */
- (BOOL)read:(nonnull NSMutableArray *)columns;

/**
 Reads a line from the opened input source.
 String Tokens are processed with the specified delimiter,
 fills in 2 string reference representing first 2 columns of a row of data
 
 @param column1 A string pointer provided to stored the value of first column
 @param column2 A string pointer provided to stored the value of second column
 @return Indicates is the opertation successed
 */
- (BOOL)read:(NSMutableString * _Nonnull * _Nonnull)column1 column2:(NSMutableString  * _Nonnull * _Nonnull)column2;

/**
 Reads one line of data from the stream,
 close the stream automatically if there is no more data to read
 
 @return String of the line was read
 */
- (nonnull NSString *)readLine;

/**
 Close the stream
 */
- (void)close;

@end
