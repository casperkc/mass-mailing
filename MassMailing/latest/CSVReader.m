//
//  CSVReader.m
//  MassMailing
//
//  Created by Casper Lee on 28/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

#import "CSVReader.h"

#import "StreamReading.h"
#import "StreamReader.h"
#import "Constants.h"

#pragma mark - Private Interface

@interface CSVReader()

@property (nonatomic, strong) id<StreamReading> reader;

@end

#pragma mark - Public Methods

@implementation CSVReader

#pragma mark Initialisers
+ (nonnull instancetype)readerForPath:(nonnull NSString *)path {
    return [[self alloc] initForPath:path];
}

- (nonnull instancetype)initForPath:(nonnull NSString *)path {
    self = [self init];
    if (self) {
        StreamReader *reader = [StreamReader reader];
        self.reader = reader;
        [self.reader openPath:path];
    }
    return self;
}

- (BOOL)read:(NSMutableString **)column1 column2:(NSMutableString **)column2 {
    NSMutableArray *array = [NSMutableArray array];
    BOOL success = [self read:array];
    
    NSString *value1 = array[FIRST_COLUMN];
    NSString *value2 = array[SECOND_COLUMN];
    
    if (value1 && [value1 isKindOfClass:[NSString class]]) {
        *column1 = value1.mutableCopy;
    }
    
    if (value2 && [value2 isKindOfClass:[NSString class]]) {
        *column2 = value2.mutableCopy;
    }
    
    return success;
}

- (BOOL)read:(NSMutableArray *)columns {
    
    BOOL success = false;
    columns[FIRST_COLUMN] = [NSNull null];
    columns[SECOND_COLUMN] = [NSNull null];
    
    NSString *line = [self.reader readLine];
    
    if (line.length > 0) {
        NSArray *splitLine = [line componentsSeparatedByString:DELIMITER];
        
        if (splitLine.count > 1) {
            columns[FIRST_COLUMN] = splitLine[FIRST_COLUMN];
            columns[SECOND_COLUMN] = splitLine[SECOND_COLUMN];
            success = true;
        }
    }
    
    return success;
}

- (NSString *)readLine {
    return [self.reader readLine];
}

- (void)close {
    [self.reader close];
}

@end
