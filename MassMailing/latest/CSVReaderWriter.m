//
//  CSVReaderWriter.m
//  MassMailing
//
//  Created by Casper Lee on 17/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

#import "CSVReaderWriter.h"
#import "CSVReader.h"
#import "CSVWriter.h"

#pragma mark - Private Interface

@interface CSVReaderWriter ()

@property (nonatomic, strong) CSVReader *reader;
@property (nonatomic, strong) CSVWriter *writer;

@end

#pragma mark - Public Methods

@implementation CSVReaderWriter

- (void)open:(NSString *)path mode:(FileMode)mode {
    switch (mode) {
        case FileModeRead: {
            CSVReader *reader = [CSVReader readerForPath:path];
            self.reader = reader;
            break;
        }
            
        case FileModeWrite: {
            CSVWriter *writer = [CSVWriter writerForPath:path];
            self.writer = writer;
            break;
        }
            
        default:
            break;
    }
}

- (BOOL)read:(NSMutableString **)column1 column2:(NSMutableString **)column2 {
    return [self.reader read:column1 column2:column2];
}

- (BOOL)read:(NSMutableArray *)columns {
    return [self.reader read:columns];
}

- (void)write:(NSArray *)columns {
    [self.writer write:columns];
}

- (void)close {
    // Writing Streams would automatically close itself when encountered end of data
    [self.reader close];
}

@end
