//
//  StreamWriting.h
//  MassMailing
//
//  Created by Casper Lee on 28/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

@protocol StreamWriting <NSObject>

/**
 Create and initialize an output stream with a repository for the data to write.
 Schedule the stream object on a run loop and open the stream.
 
 @param path The path is written data goes to
 */
- (void)openPath:(nonnull NSString *)path;

/**
 Append the `data` in the queue to be written
 
 @param data Data to write
 */
- (void)appendData:(nonnull NSData *)data;

/**
 Close and dispose the stream manually
 @warning This would probably cause data loss when data are not written into the stream
 */
- (void)close;

@end
