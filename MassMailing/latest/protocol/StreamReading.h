//
//  StreamReading.h
//  MassMailing
//
//  Created by Casper Lee on 28/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

@protocol StreamReading <NSObject>

/** -----------------------------------------------------------------------
 * @name Performing Stream Operation
 * -----------------------------------------------------------------------
 */

/**
 Create and initialize an input stream with the path provided.
 
 @param path The path of input data
 */
- (void)openPath:(nonnull NSString *)path;

/**
 Reads one line of data from the stream,
 close the stream automatically if there is no more data to read
 
 @return String of the line was read
 */
- (nonnull NSString *)readLine;

/**
 Close and dispose the stream manually
 @warning This would probably cause data loss if receiver still has
 bytes available to read
 */
- (void)close;

@end
