//
//  StreamWriter.h
//  MassMailing
//
//  Created by Casper Lee on 17/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StreamWriting.h"

/**
 * StreamWriter is a wrapper for `NSOutputStream` that implements
 * `NSStreamDelegate` to handle stream events by run-loop scheduling.
 *
 * Data are appending synchronously while the actual stream writing
 * is performed only when blocking is unlikely to take place.
 *
 * The stream would close and dispose of automatically when end of
 * stream has been reached.
 *
 * In case there is a delay in incoming data, stream would able to append
 * additional data to last opened stream.
 */

@interface StreamWriter : NSObject<StreamWriting, NSStreamDelegate>

/**-----------------------------------------------------------------------
 * @name Initialization
 * -----------------------------------------------------------------------
 */

/**
 * Creates and returns an `StreamWriter` object.
 * @return A new `StreamWriter` instance
 */
+ (nonnull instancetype)writer;

/** -----------------------------------------------------------------------
 * @name Performing Stream Operation
 * -----------------------------------------------------------------------
 */

@end
