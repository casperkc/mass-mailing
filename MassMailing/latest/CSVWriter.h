//
//  CSVWriter.h
//  MassMailing
//
//  Created by Casper Lee on 28/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSVWriter : NSObject

/**
 * Creates and returns an `CSVWriter` object.
 *
 * @param path The path of data source
 * @return A new `CSVWriter` instance
 */
+ (nonnull instancetype)writerForPath:(nonnull NSString *)path;
- (nonnull instancetype)initForPath:(nonnull NSString *)path;

/**
 * Writes a line of string representation with the specified delimiter
 * using the given values
 *
 * @param columns An array of data representing a row of data
 */
- (void)write:(nonnull NSArray *)columns;

/**
 Close and dispose the stream manually
 @warning This would probably cause data loss when data are not written into the stream
 */
- (void)close;

@end
