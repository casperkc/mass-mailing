//
//  StreamReader.h
//  MassMailing
//
//  Created by Casper Lee on 17/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StreamReading.h"

/**
 * StreamReader is a wrapper for `NSInputStream` that uses polling,
 * so each line would be returned for the further process.
 *
 * Although Run-loop scheduling is almost always preferable over polling,
 * Considering backwards compatibility must be maintained,
 * it might better suited to the threading model in the legacy code.
 *
 * The stream would close and dispose of automatically when end of
 * stream has been reached.
 */
@interface StreamReader : NSObject<StreamReading>

/**-----------------------------------------------------------------------
 * @name Initialization
 * -----------------------------------------------------------------------
 */

/**
 * Creates and returns an `StreamReader` object.
 * @return A new `StreamReader` instance
 */
+ (nonnull instancetype)reader;

@end
