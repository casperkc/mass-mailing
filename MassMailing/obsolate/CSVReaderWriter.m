/*
 A junior developer was tasked with writing a reusable implementation for a mass mailing application to read and write text files that hold tab separated data.
 
 His implementation, although it works and meets the needs of the application, is of very low quality.
 
 Your task:
 
 - Identify and annotate the shortcomings in the current implementation as if you were doing a code review, using comments in the source files.
 
 - Refactor the CSVReaderWriter implementation into clean, idiomatic, elegant, rock-solid & well performing code, without over-engineering.
 
 - Where you make trade offs, comment & explain.
 
 - Assume this code is in production and backwards compatibility must be maintained. Therefore if you decide to change the public interface, please deprecate the existing methods. Feel free to evolve the code in other ways though. You have carte blanche while respecting the above constraints. 
 */

#import <Foundation/Foundation.h>

typedef NS_OPTIONS(NSUInteger, FileMode) {
    FileModeRead = 1,
    FileModeWrite = 2
};

// Public interface should put in a header file.
// Recommends using modern Objective-C syntax for Swift compatibility
@interface CSVReaderWriter : NSObject

- (void)open:(NSString*)path mode:(FileMode)mode;
- (BOOL)read:(NSMutableString**)column1 column2:(NSMutableString**)column2;
- (BOOL)read:(NSMutableArray*)columns;
- (void)write:(NSArray*)columns;
- (void)close;

@end

@implementation CSVReaderWriter {
    // 1. By Single responsibility principle, separating stream input and output into different classes is recommended
    // 2. Consider use property instead of ivar as property is safer to read and write to from multiple threads
    //    as access to properties is also atomic by default.
    NSInputStream* inputStream;
    NSOutputStream* outputStream;
}

- (void)open:(NSString*)path mode:(FileMode)mode {
    
    // Better to use exhaustive switch for enums
    if (mode == FileModeRead) {
        inputStream = [NSInputStream inputStreamWithFileAtPath:path];
        [inputStream open];
    }
    else if (mode == FileModeWrite) {
        outputStream = [NSOutputStream outputStreamToFileAtPath:path
                                                         append:NO];
        [outputStream open];
    }
    
    // Unnecessary and undocumented expectation would cause app termination
    else {
        NSException* ex = [NSException exceptionWithName:@"UnknownFileModeException"
                                                  reason:@"Unknown file mode specified"
                                                userInfo:nil];
        @throw ex;
    }
}

// Polling is CPU-intensive, which may slow down or blocks another process like UI rendering
// Run-loop scheduling is almost always a preferable option.
// Run loops also helps to manage state.
- (NSString*)readLine {
    uint8_t ch = 0;
    NSMutableString* str = [NSMutableString string];
    while ([inputStream read:&ch maxLength:1] == 1) {
        if (ch == '\n')
            break;
        [str appendFormat:@"%c", ch];
    }
    return str;
}

// Better not limiting the ability to 2 columns only, a collection would be preferred
// Keep "DRY" - better reuse the other read method
- (BOOL)read:(NSMutableString**)column1 column2:(NSMutableString**)column2 {
    
    // Better define it in the class scope so other methods able to re-use
    int FIRST_COLUMN = 0;
    int SECOND_COLUMN = 1;
    
    NSString* line = [self readLine];
    
    if ([line length] == 0) {
        // Keeping state with fallback / error handling is better than early returns
        *column1 = nil;
        *column2 = nil;
        return false;
    }
    
    NSArray* splitLine = [line componentsSeparatedByString: @"\t"];
    
    if ([splitLine count] == 0) {
        // Keeping state with fallback / error handling is better than early returns
        *column1 = nil;
        *column2 = nil;
        return false;
    }
    else {
        *column1 = [NSMutableString stringWithString:splitLine[FIRST_COLUMN]];
        *column2 = [NSMutableString stringWithString:splitLine[SECOND_COLUMN]];
        return true;
    }
}

- (BOOL)read:(NSMutableArray*)columns {
    int FIRST_COLUMN = 0;
    int SECOND_COLUMN = 1;
    
    NSString* line = [self readLine];
    
    if ([line length] == 0) {
        // Passing nil to non-null arguments would raise runtime exception
        // false would never return
        // Should use NSNull for setting null objects
        columns[FIRST_COLUMN]=nil;
        columns[SECOND_COLUMN] = nil;
        return false;
    }
    
    // It worths to expose the delimiter with a setter, making the class reusable
    NSArray* splitLine = [line componentsSeparatedByString: @"\t"];
    
    if ([splitLine count] == 0) {
        columns[FIRST_COLUMN] = nil;
        columns[SECOND_COLUMN] = nil;
        // Keeping state with fallback / error handling is better than early returns
        return false;
    }
    else {
        columns[FIRST_COLUMN] = splitLine[FIRST_COLUMN];
        columns[SECOND_COLUMN] = splitLine[SECOND_COLUMN];
        return true;
    }
}

// Writing stream could be a blocking task, run-loop scheduling is highly recommended
// Using a buffer could also enhance performance
- (void)writeLine:(NSString*)line {
    NSData* data = [line dataUsingEncoding:NSUTF8StringEncoding];
    
    const void* bytes = [data bytes];
    [outputStream write:bytes maxLength:[data length]];
    
    unsigned char* lf = (unsigned char*)"\n";
    [outputStream write: lf maxLength: 1];
}

- (void)write:(NSArray*)columns {
    NSMutableString* outPut = [@"" mutableCopy];
    
    // `componentsJoinedByString:` could help to make syntax cleaner
    for (int i = 0; i < [columns count]; i++) {
        [outPut appendString: columns[i]];
        if (([columns count] - 1) != i) {
            [outPut appendString: @"\t"];
        }
    }
    
    [self writeLine:outPut];
}

- (void)close {
    // Stream objects should be released after close to avoid memory leak
    if (inputStream != nil) {
        [inputStream close];
    }
    if (inputStream != nil) {
        [inputStream close];
    }
}

@end
