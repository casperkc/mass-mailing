//
//  MassMailing.h
//  MassMailing
//
//  Created by Casper Lee on 18/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MassMailing.
FOUNDATION_EXPORT double MassMailingVersionNumber;

//! Project version string for MassMailing.
FOUNDATION_EXPORT const unsigned char MassMailingVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MassMailing/PublicHeader.h>

#import <MassMailing/CSVReader.h>
#import <MassMailing/CSVWriter.h>
#import <MassMailing/CSVReaderWriter.h>
