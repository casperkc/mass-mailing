//
//  CSVWriterTests.swift
//  MassMailing
//
//  Created by Casper Lee on 28/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

import Foundation

import XCTest

@testable import MassMailing

class CSVWriterTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
        MockDataProvider.reset()
    }
    
    /**
     Test the csvReaderWriter writes data properly with an array,
     verifying the written data after an amount of time
     as the write operation is performed asynchronously.
     */
    func testWrite() {
        
        let path = MockDataProvider.pathToWrite
        let csvWriter = CSVWriter(forPath: path!)
        
        let expect = expectation(description: "Write data asynchronously")
        let testValue = (NSMutableString(string: "TestColumn1"), NSMutableString(string: "TestColumn2"))
        
        csvWriter.write([testValue.0, testValue.1])
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            var column1 = NSMutableString()
            var column2 = NSMutableString()
            
            let csvReader = CSVReader(forPath: MockDataProvider.pathToWrite!)
            csvReader.read(&column1, column2: &column2)
            XCTAssertEqual(column1, testValue.0)
            XCTAssertEqual(column2, testValue.1)
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 5)
        
        MockDataProvider.reset()
    }
    
    /**
     Test the csvReaderWriter writes data properly with an array,
     verifying the written data after an amount of time
     as the write operation is performed asynchronously.
     
     And also test the ability to reopen of stream and resume writing
     in case the input stream is slow.
     */
    func testWriteWithDelay() {
        
        let expect = expectation(description: "Write data asynchronously")
        let testValueBefore = (NSMutableString(string: "TestColumn1Before"), NSMutableString(string: "TestColumn2Before"))
        let testValueLater = (NSMutableString(string: "TestColumn1Later"), NSMutableString(string: "TestColumn2Later"))
        
        let path = MockDataProvider.pathToWrite
        let csvWriter = CSVWriter(forPath: path!)
        
        csvWriter.write([testValueBefore.0, testValueBefore.1])
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            csvWriter.write([testValueLater.0, testValueLater.1])
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            let csvReader = CSVReader(forPath: MockDataProvider.pathToWrite!)
            var column1 = NSMutableString()
            var column2 = NSMutableString()
            
            csvReader.read(&column1, column2: &column2)
            XCTAssertEqual(column1, testValueBefore.0)
            XCTAssertEqual(column2, testValueBefore.1)
            
            csvReader.read(&column1, column2: &column2)
            XCTAssertEqual(column1, testValueLater.0)
            XCTAssertEqual(column2, testValueLater.1)
            
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 10)
        
        defer {
            MockDataProvider.reset()
        }
    }
}
