//
//  CSVReaderTests.swift
//  MassMailing
//
//  Created by Casper Lee on 28/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

import Foundation
import XCTest

@testable import MassMailing

class CSVReaderTests: XCTestCase {
    
    lazy var singleEntryCsvReader: CSVReader = {
        let path = MockDataProvider.path(of: .singleEntry)
        return CSVReader(forPath: path!)
    }()
    
    lazy var multipleEntriesCsvReader: CSVReader = {
        let path = MockDataProvider.path(of: .multipleEntries)
        return CSVReader(forPath: path!)
    }()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
        MockDataProvider.reset()
    }
    
    /**
     Test the csvReader reads data properly with reference of mutable strings
     with one single entry
     */
    func testReadStringColumns() {
        
        var column1 = NSMutableString()
        var column2 = NSMutableString()
        
        singleEntryCsvReader.read(&column1, column2: &column2)
        
        let row = (NSMutableStringPointer(&column1), NSMutableStringPointer(&column2))
        MockDataProvider.vaildateSingleEntry(row: row)
    }
    
    /**
     Test the csvReader reads data properly with a mutable array
     with one single entry
     */
    func testReadArrayColumns() {
        let columns = NSMutableArray()
        
        singleEntryCsvReader.read(columns)
        
        var column1 = columns[Int(FIRST_COLUMN)] as? NSMutableString
        var column2 = columns[Int(SECOND_COLUMN)] as? NSMutableString
        
        let row = (NSMutableStringPointer(&column1), NSMutableStringPointer(&column2))
        MockDataProvider.vaildateSingleEntry(row: row)
    }
    
    /**
     Test the csvReader reads data properly with a mutable array
     with multiple entries, reading from the first to the last entry.
     */
    func testMultipleArrayRows() {
        var rows = [NSArray]()
        let columns = NSMutableArray()
        
        while multipleEntriesCsvReader.read(columns) {
            rows.append(columns.copy() as! NSArray)
        }
        
        var count = rows.count
        
        var firstRowColumn1 = rows.first?[Int(FIRST_COLUMN)] as! NSMutableString
        var firstRowColumn2 = rows.first?[Int(SECOND_COLUMN)] as! NSMutableString
        
        var lastRowColumn1 = rows.last?[Int(FIRST_COLUMN)] as! NSMutableString
        var lastRowColumn2 = rows.last?[Int(SECOND_COLUMN)] as! NSMutableString
        
        let firstRow = (NSMutableStringPointer(&firstRowColumn1), NSMutableStringPointer(&firstRowColumn2))
        let lastRow = (NSMutableStringPointer(&lastRowColumn1), NSMutableStringPointer(&lastRowColumn2))
        MockDataProvider.validateMultipleEntries(count: &count, firstRow: firstRow, lastRow: lastRow)
    }
    
    /**
     Test the error handling of path opening.
     */
    func testReadInvalidPath() {
        let invalidCsvReader = CSVReader(forPath: "invalid/resource/path.type")
        let data = NSMutableArray()
        XCTAssertFalse(invalidCsvReader.read(data))
    }
}
