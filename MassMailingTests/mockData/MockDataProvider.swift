//
//  MockDataProvider.swift
//  MassMailing
//
//  Created by Casper Lee on 18/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

import Foundation
import XCTest

typealias NSMutableStringPointer = AutoreleasingUnsafeMutablePointer<NSMutableString>

class MockDataProvider {
    
    typealias Row = (NSMutableStringPointer, NSMutableStringPointer)
    
    // MARK: Defines the file name and extension
    struct File {
        
        enum Name: String {
            case output
            case singleEntry
            case multipleEntries = "100entries"
        }
        
        static let type = "csv"
    }
    // Computed variable returns: the document directory
    fileprivate static var documentDirectoryPath: String? {
        guard let docPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else {
            return nil
        }
        
        return docPath
    }
    
    
    /** Returns the path of a file in the test bundle
     - parameter file: The file name to load
     - parameter type: The file extension
     - returns: Fully qualified path of the bundled resource
    */
    class func path(of file: File.Name, ofType type: String = File.type) -> String? {
        let path = Bundle(for: self).path(forResource: file.rawValue, ofType: type)
        return path
    }
    
    // Lazy initialised variable to return the default output stream path
    static var pathToWrite: String? = {
       
        let fileManager = FileManager.default
        guard let docPath = documentDirectoryPath else {
            return nil
        }
        
        let filePath = "\(docPath)/\(File.Name.output.rawValue).\(File.type)"
        if !fileManager.fileExists(atPath: filePath) {
           fileManager.createFile(atPath: filePath, contents: nil, attributes: nil)
        }
        
        return filePath
    }()
    
    
    // Perform when tear down, to remove all test generated files
    class func reset() {
        guard let docPath = documentDirectoryPath else {
            return
        }
        
        let fileManager = FileManager.default
        guard let contents = try? fileManager.contentsOfDirectory(atPath: docPath) else {
            return
        }
        
        contents.forEach {
            try? fileManager.removeItem(atPath: $0)
        }
    }
    
    // MARK: - Validating Functions
    
    class func vaildateSingleEntry(row: Row) {
        XCTAssertEqual(row.0.pointee, "Shirley Dunn")
        XCTAssertEqual(row.1.pointee, "576 Ronald Regan Parkway")
    }
    
    class func validateMultipleEntries(count: UnsafePointer<Int>, firstRow: Row, lastRow: Row) {
        XCTAssertEqual(count.pointee, 100)
        
        XCTAssertEqual(firstRow.0.pointee, "Donald Peters")
        XCTAssertEqual(firstRow.1.pointee, "1328 Tony Way")
        
        XCTAssertEqual(lastRow.0.pointee, "Joshua Young")
        XCTAssertEqual(lastRow.1.pointee, "04721 Merry Circle")
    }
}
