//
//  CSVReaderWriterTests.swift
//  MassMailing
//
//  Created by Casper Lee on 18/10/2016.
//  Copyright © 2016 Trainline. All rights reserved.
//

import Foundation
import XCTest

@testable import MassMailing

class CSVReaderWriterTests: XCTestCase {
    
    let csvReaderWriter = CSVReaderWriter()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
        MockDataProvider.reset()
    }
    
    /**
     Test the csvReaderWriter reads data properly with reference of mutable strings
     with one single entry
     */
    func testReadStringColumns() {
        let path = MockDataProvider.path(of: .singleEntry)
        csvReaderWriter.open(path!, mode: .read)
        var column1 = NSMutableString()
        var column2 = NSMutableString()
        
        csvReaderWriter.read(&column1, column2: &column2)
        
        let row = (NSMutableStringPointer(&column1), NSMutableStringPointer(&column2))
        MockDataProvider.vaildateSingleEntry(row: row)
    }
    
    /**
     Test the csvReaderWriter reads data properly with a mutable array
     with one single entry
     */
    func testReadArrayColumns() {
        let path = MockDataProvider.path(of: .singleEntry)
        csvReaderWriter.open(path!, mode: .read)
        let columns = NSMutableArray()
        
        csvReaderWriter.read(columns)
        
        var column1 = columns[Int(FIRST_COLUMN)] as? NSMutableString
        var column2 = columns[Int(SECOND_COLUMN)] as? NSMutableString
        
        let row = (NSMutableStringPointer(&column1), NSMutableStringPointer(&column2))
        MockDataProvider.vaildateSingleEntry(row: row)
    }
    
    /**
     Test the csvReaderWriter reads data properly with a mutable array
     with multiple entries, reading from the first to the last entry.
     */
    func testMultipleArrayRows() {
        let path = MockDataProvider.path(of: .multipleEntries)
        csvReaderWriter.open(path!, mode: .read)
        var rows = [NSArray]()
        let columns = NSMutableArray()
        
        while csvReaderWriter.read(columns) {
            rows.append(columns.copy() as! NSArray)
        }
        
        var count = rows.count
        
        var firstRowColumn1 = rows.first?[Int(FIRST_COLUMN)] as! NSMutableString
        var firstRowColumn2 = rows.first?[Int(SECOND_COLUMN)] as! NSMutableString
        
        var lastRowColumn1 = rows.last?[Int(FIRST_COLUMN)] as! NSMutableString
        var lastRowColumn2 = rows.last?[Int(SECOND_COLUMN)] as! NSMutableString
        
        let firstRow = (NSMutableStringPointer(&firstRowColumn1), NSMutableStringPointer(&firstRowColumn2))
        let lastRow = (NSMutableStringPointer(&lastRowColumn1), NSMutableStringPointer(&lastRowColumn2))
        MockDataProvider.validateMultipleEntries(count: &count, firstRow: firstRow, lastRow: lastRow)
    }
    
    /**
     Test the error handling of path opening.
     */
    func testReadInvalidPath() {
        let invalidPath = "invalid/resource/path.type"
        csvReaderWriter.open(invalidPath, mode: .read)
        let data = NSMutableArray()
        XCTAssertFalse(csvReaderWriter.read(data))
    }
    
    /**
     Test the csvReaderWriter writes data properly with an array,
     verifying the written data after an amount of time
     as the write operation is performed asynchronously.
     */
    func testWrite() {
        
        let expect = expectation(description: "Write data asynchronously")
        let testValue = (NSMutableString(string: "TestColumn1"), NSMutableString(string: "TestColumn2"))
        
        let path = MockDataProvider.pathToWrite
        csvReaderWriter.open(path!, mode: .write)
        csvReaderWriter.write([testValue.0, testValue.1])
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.csvReaderWriter.open(path!, mode: .read)
            var column1 = NSMutableString()
            var column2 = NSMutableString()
            
            self.csvReaderWriter.read(&column1, column2: &column2)
            XCTAssertEqual(column1, testValue.0)
            XCTAssertEqual(column2, testValue.1)
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 5)
    }
    
    /**
     Test the csvReaderWriter writes data properly with an array,
     verifying the written data after an amount of time
     as the write operation is performed asynchronously.
     
     And also test the ability to reopen of stream and resume writing
     in case the input stream is slow.
     */
    func testWriteWithDelay() {
        
        let expect = expectation(description: "Write data asynchronously")
        let testValueBefore = (NSMutableString(string: "TestColumn1Before"), NSMutableString(string: "TestColumn2Before"))
        let testValueLater = (NSMutableString(string: "TestColumn1Later"), NSMutableString(string: "TestColumn2Later"))
        
        let path = MockDataProvider.pathToWrite
        csvReaderWriter.open(path!, mode: .write)
        csvReaderWriter.write([testValueBefore.0, testValueBefore.1])
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.csvReaderWriter.write([testValueLater.0, testValueLater.1])
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.csvReaderWriter.open(path!, mode: .read)
            var column1 = NSMutableString()
            var column2 = NSMutableString()
            
            self.csvReaderWriter.read(&column1, column2: &column2)
            XCTAssertEqual(column1, testValueBefore.0)
            XCTAssertEqual(column2, testValueBefore.1)
            
            self.csvReaderWriter.read(&column1, column2: &column2)
            XCTAssertEqual(column1, testValueLater.0)
            XCTAssertEqual(column2, testValueLater.1)
            
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 10)
    }
    
    /**
     Mesure the performance of input stream reading using polling strategy
     */
    func testReadPerformance() {
        self.measure {
            let path = MockDataProvider.path(of: .multipleEntries)
            self.csvReaderWriter.open(path!, mode: .read)
            var rows = [NSArray]()
            let columns = NSMutableArray()
            
            while self.csvReaderWriter.read(columns) {
                rows.append(columns.copy() as! NSArray)
            }
        }
    }
}
